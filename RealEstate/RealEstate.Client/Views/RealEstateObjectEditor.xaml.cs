﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using RealEstate.Client.ViewModels;
using RealEstate.DataAccess;

namespace RealEstate.Client.Views
{
    /// <summary>
    /// Interaction logic for RealEstateObjectEditor.xaml
    /// </summary>
    public partial class RealEstateObjectEditor : DXWindow
    {
        public RealEstateObjectEditor(object entity = null)
        {
            DataContext = new RealEstateObjectEditorViewModel()
            {
                Entity = entity as RealEstateObject ?? new RealEstateObject()
            };
            InitializeComponent();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
