﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace RealEstate.Client.Controls
{
    public class DoubleTextEdit : DevExpress.Xpf.Editors.TextEdit
    {
        public DoubleTextEdit()
        {
            MaskPlaceHolder = ' ';
            MaskShowPlaceHolders = false;
            MaskType = DevExpress.Xpf.Editors.MaskType.RegEx;
            Mask = @"[0-9]{1,12}([.][0-9]{1,3})?";
            this.KeyDown += (s, e) => { this.Text = this.Text.Replace(" ", string.Empty); };
        }
        
    }
}
