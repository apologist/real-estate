﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstate.Client.Controls
{
    class IntegerTextEdit : DevExpress.Xpf.Editors.TextEdit
    {
        public IntegerTextEdit()
        {
            MaskPlaceHolder = ' ';
            MaskShowPlaceHolders = false;
            MaskType = DevExpress.Xpf.Editors.MaskType.RegEx;
            Mask = @"\d{1,10}";
            this.KeyDown += (s, e) => { this.Text = this.Text.Replace(" ", string.Empty); };
        }
    }
}
