﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstate.DataAccess;
using System.Windows.Input;
using RealEstate.Client.Helpers;

namespace RealEstate.Client.ViewModels
{
    public class RealEstateObjectEditorViewModel : BaseViewModel
    {
        public RealEstateObjectEditorViewModel()
        {
            _entity = new RealEstateObject()
            {
                Country = Entities.Instance.Countries.Where(c => c.ID.Equals(1)).FirstOrDefault()
            };
            RaisePropertyChanged(() => Country);
        }

        #region Properties

        private RealEstateObject _entity;
        public RealEstateObject Entity
        {
            get { return _entity; }
            set { _entity = value; }
        }

        #region Title and promotext

        public string Title
        {
            get { return Entity.Title; }
            set
            {
                Entity.Title = value;
                RaisePropertyChanged(() => Title);
            }
        }

        public string PromoText
        {
            get { return Entity.Text; }
            set
            {
                Entity.Text = value;
                RaisePropertyChanged(() => PromoText);
            }
        }

        #endregion

        #region Base info

        public  ContractType ContractType
        {
            get { return Entity.ContractType; }
            set
            {
                Entity.ContractType = value;
                RaisePropertyChanged(() => ContractType);
            }
        }

        public  IEnumerable<ContractType> ContractTypes
        {
            get { return Entities.Instance.ContractTypes.ToList(); }
        }

        public RealtyType RealtyType
        {
            get { return Entity.RealtyType; }
            set
            {
                Entity.RealtyType = value;
                RaisePropertyChanged(() => RealtyType);
            }
        }

        public IEnumerable<RealtyType> RealtyTypes
        {
            get { return Entities.Instance.RealtyTypes.ToList(); }
        }

        public string Price
        {
            get { return (Entity.Price == null) ? string.Empty : Entity.Price.ToString(); }
            set
            {
                double d;
                if (double.TryParse(value, out d))
                    Entity.Price = d;
                RaisePropertyChanged(() => Price);
            }
        }

        public string PricePerM2
        {
            get { return (Entity.PricePerM2 == null) ? string.Empty : Entity.PricePerM2.ToString(); }
            set
            {
                int d;
                if (int.TryParse(value, out d))
                    Entity.PricePerM2 = d;
                RaisePropertyChanged(() => PricePerM2);
            }
        }

        public Currency Currency
        {
            get { return Entity.Currency; }
            set 
            {
                Entity.Currency = value;
                RaisePropertyChanged(() => Currency);
            }
        }

        public List<Currency> Currencies
        {
            get { return Entities.Instance.Currencies.ToList(); }
        }

        public string ContactName
        {
            get { return Entity.ContactName; }
            set
            {
                Entity.ContactName = value;
                RaisePropertyChanged(() => ContactName);
            }
        }

        public string Contacts
        {
            get { return Entity.Contacts; }
            set
            {
                Entity.Contacts = value;
                RaisePropertyChanged(() => Contacts);
            }
        }

        public string AgencyCode
        {
            get { return Entity.AgencyCode; }
            set
            {
                Entity.AgencyCode = value;
                RaisePropertyChanged(() => AgencyCode);
            }
        }

        public string URL
        {
            get { return Entity.URL; }
            set
            {
                Entity.URL = value;
                RaisePropertyChanged(() => URL);
            }
        }

        public bool IsMyObject
        {
            get { return Entity.IsMyObject; }
            set
            {
                Entity.IsMyObject = value;
                RaisePropertyChanged(() => IsMyObject);
            }
        }

        public bool WithoutFee
        {
            get { return Entity.WithoutFee; }
            set
            {
                Entity.WithoutFee = value;
                RaisePropertyChanged(() => WithoutFee);
            }
        }

        public bool IsOwner
        {
            get { return Entity.IsOwner; }
            set
            {
                Entity.IsOwner = value;
                RaisePropertyChanged(() => IsOwner);
            }
        }

        public bool IsNewHouse
        {
            get { return Entity.IsNewHouse; }
            set
            {
                Entity.IsNewHouse = value;
                RaisePropertyChanged(() => IsNewHouse);
            }
        }

        public bool IsPremium
        {
            get { return Entity.IsPremium; }
            set
            {
                Entity.IsPremium = value;
                RaisePropertyChanged(() => IsPremium);
            }
        }

        #endregion

        #region Address

        public Country Country
        {
            get { return Entity.Country; }
            set
            {
                Entity.Country = value;
                RaisePropertyChanged(() => Country);
            }
        }

        public IEnumerable<Country> Countries
        {
            get { return Entities.Instance.Countries.ToList(); }
        }

        public Region Region
        {
            get { return Entity.Region; }
            set
            {
                Entity.Region = value;
                RaisePropertyChanged(() => Region);
            }
        }

        public IEnumerable<Region> Regions
        {
            get { return Entities.Instance.Regions.ToList(); }
        }

        public Locality Locality
        {
            get { return Entity.Locality; }
            set
            {
                Entity.Locality = value;
                RaisePropertyChanged(() => Locality);
            }
        }

        public IEnumerable<Locality> Localities
        {
            get { return Entities.Instance.Localities.ToList(); }
        }

        public District District
        {
            get { return Entity.District; }
            set
            {
                Entity.District = value;
                RaisePropertyChanged(() => District);
            }
        }

        public IEnumerable<District> Districts
        {
            get { return Entities.Instance.Districts.ToList(); }
        }

        public MicroDistrict MicroDistrict
        {
            get { return Entity.MicroDistrict; }
            set
            {
                Entity.MicroDistrict = value;
                RaisePropertyChanged(() => MicroDistrict);
            }
        }

        public IEnumerable<MicroDistrict> MicroDistricts
        {
            get { return Entities.Instance.MicroDistricts.ToList(); }
        }

        public Street Street
        {
            get { return Entity.Street; }
            set
            {
                Entity.Street = value;
                RaisePropertyChanged(() => Street);
            }
        }

        public IEnumerable<Street> Streets
        {
            get { return Entities.Instance.Streets.ToList(); }
        }

        public string House
        {
            get { return Entity.House; }
            set
            {
                Entity.House = value;
                RaisePropertyChanged(() => House);
            }
        }

        #endregion

        #region Object Info

        public HouseType HouseType
        {
            get { return Entity.HouseType; }
            set
            {
                Entity.HouseType = value;
                RaisePropertyChanged(() => HouseType);
            }
        }

        public IEnumerable<HouseType> HouseTypes
        {
            get { return Entities.Instance.HouseTypes.ToList(); }
        }

        public RoomType RoomType
        {
            get { return Entity.RoomType; }
            set
            {
                Entity.RoomType = value;
                RaisePropertyChanged(() => RoomType);
            }
        }

        public IEnumerable<RoomType> RoomTypes
        {
            get { return Entities.Instance.RoomTypes.ToList(); }
        }

        public string RoomCount
        {
            get { return (Entity.RoomCount == null) ? string.Empty : Entity.RoomCount.ToString(); }
            set
            {
                int d;
                if (int.TryParse(value, out d))
                    Entity.RoomCount = d;
                RaisePropertyChanged(() => RoomCount);
            }
        }

        public WcType WcType
        {
            get { return Entity.WcType; }
            set
            {
                Entity.WcType = value;
                RaisePropertyChanged(() => WcType);
            }
        }

        public IEnumerable<WcType> WcTypes
        {
            get { return Entities.Instance.WcTypes.ToList(); }
        }

        public string WcCount
        {
            get { return (Entity.WcCount == null) ? string.Empty : Entity.WcCount.ToString(); }
            set
            {
                int d;
                if (int.TryParse(value, out d))
                    Entity.WcCount = d;
                RaisePropertyChanged(() => WcCount);
            }
        }

        public string FloorCount
        {
            get { return (Entity.FloorCount == null) ? string.Empty : Entity.FloorCount.ToString(); }
            set
            {
                int d;
                if (int.TryParse(value, out d))
                    Entity.FloorCount = d;
                RaisePropertyChanged(() => FloorCount);
            }
        }

        public string Floor
        {
            get { return (Entity.Floor == null) ? string.Empty : Entity.Floor.ToString(); }
            set
            {
                int d;
                if (int.TryParse(value, out d))
                    Entity.Floor = d;
                RaisePropertyChanged(() => Floor);
            }
        }

        public BalconyType BalconyType
        {
            get { return Entity.BalconyType; }
            set
            {
                Entity.BalconyType = value;
                RaisePropertyChanged(() => BalconyType);
            }
        }

        public IEnumerable<BalconyType> BalconyTypes
        {
            get { return Entities.Instance.BalconyTypes.ToList(); }
        }

        public string BalconyCount
        {
            get { return (Entity.BalconyCount == null) ? string.Empty : Entity.BalconyCount.ToString(); }
            set
            {
                int d;
                if (int.TryParse(value, out d))
                    Entity.BalconyCount = d;
                RaisePropertyChanged(() => BalconyCount);
            }
        }

        public string TotalArea
        {
            get { return (Entity.TotalArea == null) ? string.Empty : Entity.TotalArea.ToString(); }
            set
            {
                double d;
                if (double.TryParse(value, out d))
                    Entity.TotalArea = d;
                RaisePropertyChanged(() => TotalArea);
            }
        }

        public string LivingArea
        {
            get { return (Entity.LivingArea == null) ? string.Empty : Entity.LivingArea.ToString(); }
            set
            {
                double d;
                if (double.TryParse(value, out d))
                    Entity.LivingArea = d;
                RaisePropertyChanged(() => LivingArea);
            }
        }

        public string KitchenArea
        {
            get { return (Entity.KitchenArea == null) ? string.Empty : Entity.KitchenArea.ToString(); }
            set
            {
                double d;
                if (double.TryParse(value, out d))
                    Entity.KitchenArea = d;
                RaisePropertyChanged(() => KitchenArea);
            }
        }

        public string LandArea
        {
            get { return (Entity.LandArea == null) ? string.Empty : Entity.LandArea.ToString(); }
            set
            {
                double d;
                if (double.TryParse(value, out d))
                    Entity.LandArea = d;
                RaisePropertyChanged(() => LandArea);
            }
        }

        public IEnumerable<ObjectStatus> ObjectStatuses
        {
            get { return Entities.Instance.ObjectStatus.ToList(); }
        }

        public ObjectStatus ObjectStatus
        {
            get { return Entity.ObjectStatu; }
            set
            {
                Entity.ObjectStatu = value;
                RaisePropertyChanged(() => ObjectStatus);
            }
        }

        #endregion

        #endregion

        #region Commmands

        public ICommand SaveObjectCommand
        {
            get { return new DelegateCommand(SaveRealEstateObject); }
        }

        public ICommand DeleteObjectCommand
        {
            get { return new DelegateCommand(DeleteRealEstateObject, CanDeleteRealEstateObject); }
        }

        public ICommand UploadImageCommand
        {
            get { return new DelegateCommand(DeleteRealEstateObject, CanDeleteRealEstateObject); }
        }

        #endregion

        #region Methods

        private void SaveRealEstateObject()
        {
            try
            {
                Api.SaveRealEstateObject(Entity);
            }
            catch (Exception ex)
            {

            }
        }

        private void DeleteRealEstateObject()
        {
            Api.DeleteRealEstateObject(Entity);
        }

        private bool CanDeleteRealEstateObject()
        {
            if(Entity != null)
                return Entities.Instance.RealEstateObjects.Find(Entity.ID) != null;
            return false;
        }

        private void OpenFileDialog()
        {

        }

        #endregion



    }
}
