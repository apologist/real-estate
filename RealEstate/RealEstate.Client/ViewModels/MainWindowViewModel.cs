﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;
using System.Windows.Input;
using RealEstate.DataAccess;
using RealEstate.Client.Helpers;
using RealEstate.Client.Views;
using System.IO;

namespace RealEstate.Client.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        public MainWindowViewModel()
        {
            Predicate = FilterAllRealEstateObjects;
        }

        #region Properties

        public ObservableCollection<RealEstateObject>  RealEstateObjects
        {
            get { return new ObservableCollection<RealEstateObject>(Entities.Instance.RealEstateObjects.AsEnumerable().Where(Predicate)); }
        }
        Func<RealEstateObject, bool> _predicate;
        Func<RealEstateObject, bool> Predicate
        {
            get { return _predicate; }
            set { _predicate = value; }
        }

        private bool FilterAllRealEstateObjects(RealEstateObject obj)
        {
            return true;
        }

        private bool FilterSaleRealEstateObjects(RealEstateObject obj)
        {
            return obj.ContractType.Code == "sale";
        }

        private bool FilterPurchaseRealEstateObjects(RealEstateObject obj)
        {
            return obj.ContractType.Code == "purchase";
        }

        private object _focusedRow;

        public object FocusedRow
        {
            get { return _focusedRow; }
            set { _focusedRow = value; }
        }
        

        #endregion

        #region Commands

        public ICommand CreateNewRealEstateObjectCommand
        {
            get { return new DelegateCommand(() => CreateNewRealEstateObject()); }
        }

        public ICommand OpenRealEstateObjectCommand
        {
            get { return new DelegateCommand((p) => OpenRealEstateObject(p as RealEstateObject)); }
        }

        public ICommand DeleteRealEstateObjectCommand
        {
            get { return new DelegateCommand((p) => DeleteRealEstateObject(p as RealEstateObject)); }
        }

        public ICommand ExportRealEstateObjectsToXmlForLunCommand
        {
            get { return new DelegateCommand(() => ExportRealEstateObjectsToXmlForLun()); }
        }

        #endregion

        #region Methods

        private void CreateNewRealEstateObject()
        {
            var editor = new RealEstateObjectEditor();
            editor.Closed += (s, e) => 
            { 
                RaisePropertyChanged(() => RealEstateObjects); 
            };
            editor.Show();
        }

        private void DeleteRealEstateObject(RealEstateObject realEstateObject)
        {
            Api.DeleteRealEstateObject(realEstateObject);
            RaisePropertyChanged(() => RealEstateObjects);
        }

        private bool CanDeleteRealEstateObject()
        {
            return FocusedRow != null;
        }

        private void OpenRealEstateObject(RealEstateObject realEstateObject)
        {
            var editor = new RealEstateObjectEditor(realEstateObject);
            editor.Closed += (s, e) =>
            {
                RaisePropertyChanged(() => RealEstateObjects);
            };
            editor.Show();
        }

        private void ExportRealEstateObjectsToXmlForLun()
        {
            XmlDocument doc = new XmlDocument();
            var realEstateObjects = Entities.Instance.RealEstateObjects.AsEnumerable();
            const string dateFormat = @"yyyy-M-d HH:mm:ss";

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            XmlElement pageElement = doc.CreateElement(string.Empty, "page", "http://www.w3.org/2001/XMLSchema-instance");
            doc.AppendChild(pageElement);

            XmlElement generationTime = doc.CreateElement(string.Empty, "generation_time", string.Empty);
            generationTime.AppendChild(doc.CreateTextNode(DateTime.Now.ToString(dateFormat)));
            pageElement.AppendChild(generationTime);

            XmlElement announcements = doc.CreateElement(string.Empty, "announcements", string.Empty);
            pageElement.AppendChild(announcements);

            foreach (var realEstateObject in realEstateObjects)
            {
                XmlElement announcement = doc.CreateElement(string.Empty, "announcement", string.Empty);

                XmlElement add_time = doc.CreateElement(string.Empty, "add_time", string.Empty);
                add_time.AppendChild(doc.CreateTextNode(realEstateObject.AddTime.ToString(dateFormat)));
                announcement.AppendChild(add_time);

                XmlElement update_time = doc.CreateElement(string.Empty, "update_time", string.Empty);
                update_time.AppendChild(doc.CreateTextNode(realEstateObject.UpdateTime.ToString(dateFormat)));
                announcement.AppendChild(update_time);

                XmlElement contract_type = doc.CreateElement(string.Empty, "contract_type", string.Empty);
                contract_type.AppendChild(doc.CreateTextNode(realEstateObject.ContractType.Name));
                announcement.AppendChild(contract_type);

                XmlElement realty_type = doc.CreateElement(string.Empty, "realty_type", string.Empty);
                realty_type.AppendChild(doc.CreateTextNode(realEstateObject.RealtyType.Name));
                announcement.AppendChild(realty_type);

                if (realEstateObject.Region != null && realEstateObject.Region.Name.Trim() != string.Empty)
                {
                    XmlElement region = doc.CreateElement(string.Empty, "region", string.Empty);
                    region.AppendChild(doc.CreateTextNode(realEstateObject.Region.Name));
                    announcement.AppendChild(region);
                }

                XmlElement city = doc.CreateElement(string.Empty, "city", string.Empty);
                city.AppendChild(doc.CreateTextNode(realEstateObject.Locality.Name));
                announcement.AppendChild(city);                

                if (realEstateObject.District != null && realEstateObject.District.Name.Trim() != string.Empty)
                {
                    XmlElement district = doc.CreateElement(string.Empty, "district", string.Empty);
                    district.AppendChild(doc.CreateTextNode(realEstateObject.District.Name));
                    announcement.AppendChild(district);
                }

                if (realEstateObject.MicroDistrict != null && realEstateObject.MicroDistrict.Name.Trim() != string.Empty)
                {
                    XmlElement microdistrict = doc.CreateElement(string.Empty, "microdistrict", string.Empty);
                    microdistrict.AppendChild(doc.CreateTextNode(realEstateObject.MicroDistrict.Name));
                    announcement.AppendChild(microdistrict);
                }

                if (realEstateObject.Street != null && realEstateObject.Street.Name.Trim() != string.Empty)
                {
                    XmlElement street = doc.CreateElement(string.Empty, "street", string.Empty);
                    street.AppendChild(doc.CreateTextNode(realEstateObject.Street.Name));
                    announcement.AppendChild(street);
                }

                if (realEstateObject.House != null && realEstateObject.House.Trim() != string.Empty)
                {
                    XmlElement house = doc.CreateElement(string.Empty, "house", string.Empty);
                    house.AppendChild(doc.CreateTextNode(realEstateObject.House));
                    announcement.AppendChild(house);
                }

                if (realEstateObject.RoomCount != null)
                {
                    XmlElement room_count = doc.CreateElement(string.Empty, "room_count", string.Empty);
                    room_count.AppendChild(doc.CreateTextNode(realEstateObject.RoomCount.ToString()));
                    announcement.AppendChild(room_count);
                }

                if (realEstateObject.RoomType != null && realEstateObject.RoomType.Name.Trim() != string.Empty)
                {
                    XmlElement room_type = doc.CreateElement(string.Empty, "room_type", string.Empty);
                    room_type.AppendChild(doc.CreateTextNode(realEstateObject.RoomType.Name));
                    announcement.AppendChild(room_type);
                }

                if (realEstateObject.Floor != null)
                {
                    XmlElement floor = doc.CreateElement(string.Empty, "floor", string.Empty);
                    floor.AppendChild(doc.CreateTextNode(realEstateObject.Floor.ToString()));
                    announcement.AppendChild(floor);
                }

                if (realEstateObject.FloorCount != null)
                {
                    XmlElement floor_count = doc.CreateElement(string.Empty, "floor_count", string.Empty);
                    floor_count.AppendChild(doc.CreateTextNode(realEstateObject.FloorCount.ToString()));
                    announcement.AppendChild(floor_count);
                }

                if (realEstateObject.TotalArea != null)
                {
                    XmlElement total_area = doc.CreateElement(string.Empty, "total_area", string.Empty);
                    total_area.AppendChild(doc.CreateTextNode(realEstateObject.TotalArea.ToString()));
                    announcement.AppendChild(total_area);
                }

                if (realEstateObject.LivingArea != null)
                {
                    XmlElement living_area = doc.CreateElement(string.Empty, "living_area", string.Empty);
                    living_area.AppendChild(doc.CreateTextNode(realEstateObject.LivingArea.ToString()));
                    announcement.AppendChild(living_area);
                }

                if (realEstateObject.KitchenArea != null)
                {
                    XmlElement kitchen_area = doc.CreateElement(string.Empty, "kitchen_area", string.Empty);
                    kitchen_area.AppendChild(doc.CreateTextNode(realEstateObject.KitchenArea.ToString()));
                    announcement.AppendChild(kitchen_area);
                }

                if (realEstateObject.LandArea != null)
                {
                    XmlElement land_area = doc.CreateElement(string.Empty, "land_area", string.Empty);
                    land_area.AppendChild(doc.CreateTextNode(realEstateObject.LandArea.ToString()));
                    announcement.AppendChild(land_area);
                }

                if (realEstateObject.Price != null)
                {
                    XmlElement price = doc.CreateElement(string.Empty, "price", string.Empty);
                    price.AppendChild(doc.CreateTextNode(realEstateObject.Price.ToString()));
                    announcement.AppendChild(price);
                }

                if (realEstateObject.Currency != null && realEstateObject.Currency.Name.Trim() != string.Empty)
                {
                    XmlElement currency = doc.CreateElement(string.Empty, "currency", string.Empty);
                    currency.AppendChild(doc.CreateTextNode(realEstateObject.Currency.Name.ToString()));
                    announcement.AppendChild(currency);
                }

                if (realEstateObject.HouseType != null && realEstateObject.HouseType.Name.Trim() != string.Empty)
                {
                    XmlElement house_type = doc.CreateElement(string.Empty, "house_type", string.Empty);
                    house_type.AppendChild(doc.CreateTextNode(realEstateObject.HouseType.Name.ToString()));
                    announcement.AppendChild(house_type);
                }

                if (realEstateObject.WcType != null && realEstateObject.WcType.Name.Trim() != string.Empty)
                {
                    XmlElement wc_type = doc.CreateElement(string.Empty, "wc_type", string.Empty);
                    wc_type.AppendChild(doc.CreateTextNode(realEstateObject.WcType.Name.ToString()));
                    announcement.AppendChild(wc_type);
                }

                if (realEstateObject.Title != null && realEstateObject.Title.Trim() != string.Empty)
                {
                    XmlElement title = doc.CreateElement(string.Empty, "title", string.Empty);
                    title.AppendChild(doc.CreateTextNode(realEstateObject.Title));
                    announcement.AppendChild(title);
                }

                if (realEstateObject.Text != null && realEstateObject.Text.Trim() != string.Empty)
                {
                    XmlElement text = doc.CreateElement(string.Empty, "text", string.Empty);
                    text.AppendChild(doc.CreateTextNode(realEstateObject.Text));
                    announcement.AppendChild(text);
                }

                if (realEstateObject.Contacts != null && realEstateObject.Contacts.Trim() != string.Empty)
                {
                    XmlElement contacts = doc.CreateElement(string.Empty, "contacts", string.Empty);
                    contacts.AppendChild(doc.CreateTextNode(realEstateObject.Contacts));
                    announcement.AppendChild(contacts);
                }

                if (realEstateObject.ContactName != null && realEstateObject.ContactName.Trim() != string.Empty)
                {
                    XmlElement contact_name = doc.CreateElement(string.Empty, "contact_name", string.Empty);
                    contact_name.AppendChild(doc.CreateTextNode(realEstateObject.ContactName));
                    announcement.AppendChild(contact_name);
                }

                XmlElement without_fee = doc.CreateElement(string.Empty, "without_fee", string.Empty);
                without_fee.AppendChild(doc.CreateTextNode(realEstateObject.WithoutFee.ToString()));
                announcement.AppendChild(without_fee);
                
                XmlElement is_owner = doc.CreateElement(string.Empty, "is_owner", string.Empty);
                is_owner.AppendChild(doc.CreateTextNode(realEstateObject.IsOwner.ToString()));
                announcement.AppendChild(is_owner);
                
                XmlElement is_new_house = doc.CreateElement(string.Empty, "is_new_house", string.Empty);
                is_new_house.AppendChild(doc.CreateTextNode(realEstateObject.IsNewHouse.ToString()));
                announcement.AppendChild(is_new_house);
                
                XmlElement is_premium = doc.CreateElement(string.Empty, "is_premium", string.Empty);
                is_premium.AppendChild(doc.CreateTextNode(realEstateObject.IsPremium.ToString()));
                announcement.AppendChild(is_premium);

                if (realEstateObject.AgencyCode != null && realEstateObject.AgencyCode.Trim() != string.Empty)
                {
                    XmlElement agency_code = doc.CreateElement(string.Empty, "agency_code", string.Empty);
                    agency_code.AppendChild(doc.CreateTextNode(realEstateObject.AgencyCode));
                    announcement.AppendChild(agency_code);
                }

                if (realEstateObject.URL != null && realEstateObject.URL.Trim() != string.Empty)
                {
                    XmlElement url = doc.CreateElement(string.Empty, "url", string.Empty);
                    url.AppendChild(doc.CreateTextNode(realEstateObject.URL));
                    announcement.AppendChild(url);
                }

                //if (realEstateObject.RealEstateObjectImages.Count > 0)
                //{
                //    XmlElement images = doc.CreateElement("images");
                //    announcement.AppendChild(images);

                //    foreach (var image in realEstateObject.RealEstateObjectImages.AsEnumerable())
                //    {
                //        if (image.ImageData.Length != 0)
                //        {
                //            XmlElement imageElement = doc.CreateElement(string.Empty, "image", string.Empty);
                //            imageElement.AppendChild(doc.CreateTextNode(image.ID.ToString()+image.Extension));
                //            announcement.AppendChild(imageElement);
                //        }
                //    }
                //}

                announcements.AppendChild(announcement);
            }
            doc.Save("S:\\document.xml");
        }

        #endregion

    }
}
