﻿
SET QUOTED_IDENTIFIER ON

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[Options]'))
BEGIN
  CREATE TABLE dbo.[Options]
  (
    ID INT IDENTITY NOT NULL,
    SchemaVersion NUMERIC(19, 2) NOT NULL,
    VersionDateTime DATETIME NOT NULL CONSTRAINT DV_Options_VersionDateTime DEFAULT (getdate()),
    CONSTRAINT PK_Options PRIMARY KEY CLUSTERED (ID)
  )
END

DECLARE @schema_version NUMERIC(19, 2)

BEGIN TRY

SET @schema_version = 1
IF NOT EXISTS (SELECT NULL FROM dbo.[Options] WHERE [SchemaVersion] = @schema_version)
BEGIN
  BEGIN TRANSACTION
	
	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[Country]'))
		BEGIN
			CREATE TABLE dbo.Country
			(
				ID int IDENTITY(1,1) NOT NULL,
				Name varchar(255) NOT NULL,
				CONSTRAINT PK_Country PRIMARY KEY (ID)
			)
		END

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[Region]'))
		BEGIN
			CREATE TABLE dbo.Region
			(
				ID int IDENTITY(1,1) NOT NULL,
				Name varchar(255) NOT NULL,
				CountryID int NOT NULL,
				CONSTRAINT PK_Region PRIMARY KEY (ID),
				CONSTRAINT FK_RegionCountryID FOREIGN KEY(CountryID)
					REFERENCES dbo.Country(ID),
			)
		END

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[LocalityType]'))
		BEGIN
			CREATE TABLE dbo.LocalityType
			(
				ID int IDENTITY(1,1) NOT NULL,
				Code varchar(30) NOT NULL,
				Name varchar(255) NOT NULL,
				CONSTRAINT PK_LocalityType PRIMARY KEY (ID)
			)
		END

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[Locality]'))
		BEGIN
			CREATE TABLE dbo.Locality
			(
				ID int IDENTITY(1,1) NOT NULL,
				Name varchar(255) NOT NULL,
				TypeID int NOT NULL,
				CountryID int NOT NULL,
				RegionID int NULL,
				CONSTRAINT PK_Locality PRIMARY KEY (ID),
				CONSTRAINT FK_LocalityCountryID FOREIGN KEY(CountryID)
					REFERENCES dbo.Country(ID),
				CONSTRAINT FK_LocalityRegionID FOREIGN KEY(RegionID)
					REFERENCES dbo.Region(ID),
				CONSTRAINT FK_LocalityLocalityTypeID FOREIGN KEY(TypeID)
					REFERENCES dbo.LocalityType(ID)
			)
		END

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[District]'))
		BEGIN
			CREATE TABLE dbo.District
			(
				ID int IDENTITY(1,1) NOT NULL,
				Name varchar(255) NOT NULL,
				LocalityID int NOT NULL,
				CONSTRAINT PK_District PRIMARY KEY (ID),
				CONSTRAINT FK_DistrictLocalityID FOREIGN KEY(LocalityID)
					REFERENCES dbo.Locality(ID)
			)
		END

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[MicroDistrict]'))
		BEGIN
			CREATE TABLE dbo.MicroDistrict
			(
				ID int IDENTITY(1,1) NOT NULL,
				Name varchar(255) NOT NULL,
				DistrictID int NOT NULL,
				CONSTRAINT PK_MicroDistrict PRIMARY KEY (ID),
				CONSTRAINT FK_MicroDistrictDistrictID FOREIGN KEY(DistrictID)
					REFERENCES dbo.District(ID)
			)
		END

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[Street]'))
		BEGIN
			CREATE TABLE dbo.Street
			(
				ID int IDENTITY(1,1) NOT NULL,
				Name varchar(255) NOT NULL,
				LocalityID int NOT NULL,
				DistrictID int NULL,
				MicroDistrictID int NULL,
				CONSTRAINT PK_Street PRIMARY KEY (ID),
				CONSTRAINT FK_StreetLocalityID FOREIGN KEY(LocalityID)
					REFERENCES dbo.Locality(ID),
				CONSTRAINT FK_StreetDistrictID FOREIGN KEY(DistrictID)
					REFERENCES dbo.District(ID),
				CONSTRAINT FK_StreetMicroDistrictID FOREIGN KEY(MicroDistrictID)
					REFERENCES dbo.MicroDistrict(ID),
			)
		END

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[ContractType]'))
		BEGIN
			CREATE TABLE dbo.ContractType
			(
				ID int IDENTITY(1,1) NOT NULL,
				Code varchar(30) NOT NULL,
				Name varchar(255) NOT NULL,
				CONSTRAINT PK_ContractType PRIMARY KEY (ID)
			)
		END

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[RealtyType]'))
		BEGIN
			CREATE TABLE dbo.RealtyType
			(
				ID int IDENTITY(1,1) NOT NULL,
				Code varchar(30) NOT NULL,
				Name varchar(255) NOT NULL,
				CONSTRAINT PK_RealtyType PRIMARY KEY (ID)
			)
		END

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[RoomType]'))
		BEGIN
			CREATE TABLE dbo.RoomType
			(
				ID int IDENTITY(1,1) NOT NULL,
				Code varchar(30) NOT NULL,
				Name varchar(255) NOT NULL,
				CONSTRAINT PK_RoomType PRIMARY KEY (ID)
			)
		END
	
	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[HouseType]'))
		BEGIN
			CREATE TABLE dbo.HouseType
			(
				ID int IDENTITY(1,1) NOT NULL,
				Code varchar(30) NOT NULL,
				Name varchar(255) NOT NULL,
				CONSTRAINT PK_HouseType PRIMARY KEY (ID)
			)
		END

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[WcType]'))
		BEGIN
			CREATE TABLE dbo.WcType
			(
				ID int IDENTITY(1,1) NOT NULL,
				Code varchar(30) NOT NULL,
				Name varchar(255) NOT NULL,
				CONSTRAINT PK_WcType PRIMARY KEY (ID)
			)
		END

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[BalconyType]'))
		BEGIN
			CREATE TABLE dbo.BalconyType
			(
				ID int IDENTITY(1,1) NOT NULL,
				Code varchar(30) NOT NULL,
				Name varchar(255) NOT NULL,
				CONSTRAINT PK_BalconyType PRIMARY KEY (ID)
			)
		END

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[Currency]'))
		BEGIN
			CREATE TABLE dbo.Currency
			(
				ID int IDENTITY(1,1) NOT NULL,
				Code varchar(30) NOT NULL,
				Name varchar(255) NOT NULL,
				CONSTRAINT PK_Currency PRIMARY KEY (ID)
			)
		END

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[ObjectStatus]'))
		BEGIN
			CREATE TABLE dbo.ObjectStatus
			(
				ID int IDENTITY(1,1) NOT NULL,
				Code varchar(30) NOT NULL,
				Name varchar(255) NOT NULL,
				CONSTRAINT PK_ObjectStatus PRIMARY KEY (ID)
			)
		END

	IF NOT EXISTS (SELECT * FROM sys.tables WHERE object_id = object_id('dbo.RealEstateObject'))
		BEGIN
			CREATE TABLE dbo.RealEstateObject
			(
				ID int IDENTITY(1,1) NOT NULL,
				AddTime datetime2 NOT NULL,
				UpdateTime datetime2 NOT NULL,
				ObjectStatusID int NOT NULL,
				ContractTypeID int NOT NULL,
				RealtyTypeID int NOT NULL,
				CountryID int NOT NULL,
				RegionID int NULL,
				LocalityID int NOT NULL,
				DistrictID int NULL,
				MicroDistrictID int NULL,
				StreetID int NULL,
				House nvarchar(30) NULL,
				RoomCount int NULL,
				RoomTypeID int NULL,
				[Floor] int NULL,
				FloorCount int NULL,
				TotalArea float NULL,
				LivingArea float NULL,
				KitchenArea float NULL,
				LandArea float NULL,
				Price float NULL,
				PricePerM2 int NULL,
				CurrencyID int NULL,
				HouseTypeID int NULL,
				BalconyTypeID int NULL,
				BalconyCount int NULL,
				WcTypeID int NULL,
				WcCount int NULL,
				Title nvarchar(255) NOT NULL,
				[Text] nvarchar(max) NULL,
				Contacts nvarchar(max) NULL, --контактные данные (телефоны, email)
				ContactName nvarchar(max) NULL, --имя физического/юридического лица, которому принадлежат вышеуказанные контактные данные
				WithoutFee bit NOT NULL DEFAULT(0), --претендует ли автор объявления на комиссию за услуги
				IsOwner bit NOT NULL DEFAULT(0), --является ли физическое/юридическое лицо, которому принадлежат вышеуказанные контактные данные, полноправным владельцем данного объекта недвижимости
				IsNewHouse bit NOT NULL DEFAULT(0),
				IsPremium bit NOT NULL DEFAULT(0),
				AgencyCode nvarchar(30) NOT NULL,
				URL nvarchar(max) NULL,
				CONSTRAINT PK_RealEstateObject PRIMARY KEY(ID),
				CONSTRAINT FK_RealEstateObjectObjectStatusID FOREIGN KEY(ObjectStatusID)
					REFERENCES dbo.ObjectStatus(ID),
				CONSTRAINT FK_RealEstateObjectCurrencyID FOREIGN KEY(CurrencyID)
					REFERENCES dbo.Currency(ID),
				CONSTRAINT FK_RealEstateObjectContractTypeID FOREIGN KEY(ContractTypeID)
					REFERENCES dbo.ContractType(ID),
				CONSTRAINT FK_RealEstateObjectRealtyTypeID FOREIGN KEY(RealtyTypeID)
					REFERENCES dbo.RealtyType(ID),
				CONSTRAINT FK_RealEstateObjectRoomTypeID FOREIGN KEY(RoomTypeID)
					REFERENCES dbo.RoomType(ID),
				CONSTRAINT FK_RealEstateObjectHouseTypeID FOREIGN KEY(HouseTypeID)
					REFERENCES dbo.HouseType(ID),
				CONSTRAINT FK_RealEstateObjectWcTypeID FOREIGN KEY(WcTypeID)
					REFERENCES dbo.WcType(ID),
				CONSTRAINT FK_RealEstateObjectBalconyTypeID FOREIGN KEY(BalconyTypeID)
					REFERENCES dbo.BalconyType(ID),
				CONSTRAINT FK_RealEstateObjectCountryID FOREIGN KEY(CountryID)
					REFERENCES dbo.Country(ID),
				CONSTRAINT FK_RealEstateObjectRegionID FOREIGN KEY(RegionID)
					REFERENCES dbo.Region(ID),
				CONSTRAINT FK_RealEstateObjectLocalityID FOREIGN KEY(LocalityID)
					REFERENCES dbo.Locality(ID),
				CONSTRAINT FK_RealEstateObjectDistrictID FOREIGN KEY(DistrictID)
					REFERENCES dbo.District(ID),
				CONSTRAINT FK_RealEstateObjectMicroDistrictID FOREIGN KEY(MicroDistrictID)
					REFERENCES dbo.MicroDistrict(ID),
				CONSTRAINT FK_RealEstateObjectStreetID FOREIGN KEY(StreetID)
					REFERENCES dbo.Street(ID),
			)
		END

  INSERT INTO dbo.[Options] ([SchemaVersion]) VALUES (@schema_version)
  COMMIT
END


SET @schema_version = 2
IF NOT EXISTS (SELECT NULL FROM dbo.[Options] WHERE [SchemaVersion] = @schema_version)
BEGIN

	EXEC sp_configure filestream_access_level, 2
	RECONFIGURE

	DECLARE @dbName varchar(50) = CAST ((SELECT DB_NAME()) AS varchar(50))
		DECLARE @fsName varchar(50) = 'fs' + @dbName;
		DECLARE @dbPath varchar (255)
		SELECT @dbPath = physical_name FROM sys.master_files WHERE database_id = DB_ID(@dbName) AND type_desc = 'ROWS'
		set @dbPath = REVERSE(RIGHT(REVERSE(@dbPath),(LEN(@dbPath)-CHARINDEX('\', REVERSE(@dbPath),1))+1))
		DECLARE @filesPath varchar(255) = @dbPath + 'FileStream' + @dbName
		print @dbName
		print @fsName
		print @dbPath
		print @filesPath
		exec ('
			IF NOT EXISTS(SELECT * FROM sys.filegroups WHERE name = ''' + @fsName +''')
			BEGIN
				ALTER DATABASE ' + @dbName + '
				ADD FILEGROUP ' + @fsName + '
				CONTAINS FILESTREAM
			END
		')

		exec ('
			IF NOT EXISTS(SELECT * FROM sys.database_files WHERE name = ''' + @fsName + ''')
			BEGIN
				ALTER DATABASE ' + @dbName + '
				ADD FILE (NAME=''' + @fsName + ''', FILENAME=''' + @filesPath + ''')
				TO FILEGROUP ' + @fsName + '
			END
		')

  BEGIN TRANSACTION

	IF NOT EXISTS(SELECT * FROM sys.all_columns WHERE name = 'IsMyObject' AND object_id=OBJECT_ID('dbo.RealEstateObject'))
		ALTER TABLE dbo.RealEstateObject ADD IsMyObject bit NOT NULL DEFAULT(0);


	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.[RealEstateObjectImage]'))
		BEGIN
			CREATE TABLE dbo.RealEstateObjectImage
			(
				ID int IDENTITY(1,1) NOT NULL,
				RealEstateObjectID int NOT NULL,
				ImageData varbinary(max) FILESTREAM  NOT NULL,
				Extension varchar(6) NOT NULL,
				LastUpdate datetime2(7) NOT NULL DEFAULT (SYSDATETIME()),
				IsMainImage bit NOT NULL DEFAULT ((0)),
				RowGuid uniqueidentifier ROWGUIDCOL  NOT NULL UNIQUE DEFAULT NEWID(),
				CONSTRAINT PK_RealEstateObjectImage PRIMARY KEY (ID),
				CONSTRAINT FK_ImageRealEstateObjectID FOREIGN KEY(RealEstateObjectID)
					REFERENCES dbo.RealEstateObject(ID)
			)
		END

  INSERT INTO dbo.[Options] ([SchemaVersion]) VALUES (@schema_version)
  COMMIT
END

END TRY
BEGIN CATCH
	DECLARE @ErrMessage VARCHAR(MAX)
	SELECT @ErrMessage = 'Error line ' + cast(error_line() AS VARCHAR(10)) + ' - ' + error_message() + '. schema_version = ' + cast(@schema_version AS VARCHAR(10))
	ROLLBACK
	RAISERROR (@ErrMessage, 17, 1)
END CATCH	