﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstate.DataAccess
{
    public class Api
    {
        public static void Save(object entity)
        {
            var context = Entities.Instance.Set(entity.GetType());
            string keyPropertyName = @"ID";

            if (entity!= null && entity.GetType().GetProperties().Where(p => p.Name == keyPropertyName).Count() == 1)
            {
                long key = -1;
                long.TryParse(entity.GetType().GetProperty(keyPropertyName).GetValue(entity).ToString(), out key);
                if (key == 0)
                {
                    context.Add(entity);
                    Entities.SaveAllChanges();
                }
                else if (key > 0)
                {
                    Entities.SaveAllChanges();
                }   
            }
        }

        public static void Delete(object entity)
        {
            var context = Entities.Instance.Set(entity.GetType());
            string keyPropertyName = @"ID";

            if (entity != null && entity.GetType().GetProperties().Where(p => p.Name == keyPropertyName).Count() == 1)
            {
                long key = -1;
                long.TryParse(entity.GetType().GetProperty(keyPropertyName).GetValue(entity).ToString(), out key);
                if (key > 0)
                {
                    if (context.Find(key) != null)
                    {
                        context.Remove(entity);
                        Entities.SaveAllChanges();
                        Entities.Instance.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
                    }
                }

            }
        }

        public static void SaveRealEstateObject(RealEstateObject realEstateObject)
        {
            if(realEstateObject != null)
            {
                realEstateObject.UpdateTime = DateTime.Now;
                if (Entities.Instance.RealEstateObjects.Find(realEstateObject.ID) != null)
                {
                    Entities.SaveAllChanges();
                }
                else
                {
                    realEstateObject.AddTime = DateTime.Now;
                    Entities.Instance.RealEstateObjects.Add(realEstateObject);
                    Entities.SaveAllChanges();
                }
            }
        }

        public static void DeleteRealEstateObject(RealEstateObject realEstateObject)
        {
            if (realEstateObject != null && Entities.Instance.RealEstateObjects.Find(realEstateObject.ID) != null)
            {
                try
                {
                    Entities.Instance.RealEstateObjects.Remove(realEstateObject);
                    Entities.SaveAllChanges();
                    Entities.Instance.Entry(realEstateObject).State = System.Data.Entity.EntityState.Deleted;
                }
                catch(Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }

        }

        public static IEnumerable<Country> GetCountries()
        {
            return Entities.Instance.Countries.ToList();
        }

        public static IEnumerable<Region> GetRegions()
        {
            return Entities.Instance.Regions.ToList();
        }

        public static IEnumerable<Locality> GetLocalities()
        {
            return Entities.Instance.Localities.ToList();
        }

        public static IEnumerable<LocalityType> GetLocalityTypes()
        {
            return Entities.Instance.LocalityTypes.ToList();
        }

        public static IEnumerable<District> GetDistricts()
        {
            return Entities.Instance.Districts.ToList();
        }

        public static IEnumerable<MicroDistrict> GetMicroDistricts()
        {
            return Entities.Instance.MicroDistricts.ToList();
        }

        public static IEnumerable<Street> GetStreets()
        {
            return Entities.Instance.Streets.ToList();
        }

        public static IEnumerable<ObjectStatus> GetObjectStatuses()
        {
            return Entities.Instance.ObjectStatus.ToList();
        }

        public static IEnumerable<Currency> GetCurrencies()
        {
            return Entities.Instance.Currencies.ToList();
        }

        public static IEnumerable<ContractType> GetContractTypes()
        {
            return Entities.Instance.ContractTypes.ToList();
        }

        public static IEnumerable<RealtyType> GetRealtyTypes()
        {
            return Entities.Instance.RealtyTypes.ToList();
        }

        public static IEnumerable<RoomType> GetRoomTypes()
        {
            return Entities.Instance.RoomTypes.ToList();
        }

        public static IEnumerable<HouseType> GetHouseTypes()
        {
            return Entities.Instance.HouseTypes.ToList();
        }

        public static IEnumerable<WcType> GetWcTypes()
        {
            return Entities.Instance.WcTypes.ToList();
        }

        public static IEnumerable<BalconyType> GetBalconyTypes()
        {
            return Entities.Instance.BalconyTypes.ToList();
        }
    }
}
