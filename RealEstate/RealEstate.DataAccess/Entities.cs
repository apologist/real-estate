﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstate.DataAccess
{
    public class Entities : RealEstateEntities
    {
        private static Entities _instance;
        public static Entities Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Entities();
                return _instance;
            }
        }

        public static void Reload()
        {
            _instance = new Entities();
        }

        public static void SaveAllChanges()
        {
            bool saveFailed;
            do
            {
                saveFailed = false;

                try
                {
                    Instance.SaveChanges();
                }
                catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
                {
                    saveFailed = true;
                    ex.Entries.Single().Reload();
                }
            } while (saveFailed);
        }

        

    }
}
