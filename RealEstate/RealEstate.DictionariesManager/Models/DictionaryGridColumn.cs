﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstate.DictionariesManager.Models
{
    public class DictionaryGridColumn
    {
        public DictionaryGridColumn(string header, string displayMemberBinding)
        {
            _header = header;
            _displayMemberBinding = displayMemberBinding;
        }

        private double _width;
        public double Width
        {
            get { return _width; }
            set { _width = value; }
        }

        private string _header;
        public string Header
        {
            get { return _header; }
            set { _header = value; }
        }

        private string _displayMemberBinding;
        public string DisplayMemberBinding
        {
            get { return _displayMemberBinding; }
            set { _displayMemberBinding = value; }
        }
        
        

    }
}
