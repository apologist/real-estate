﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstate.DictionariesManager.Models
{
    public class StandardDictionary : SimpleDictionary
    {
        public StandardDictionary(Type entityType) : base(entityType)
        {
            DefaultColumns.Add(new DictionaryGridColumn("Название", "Name"));
            DefaultColumns.Add(new DictionaryGridColumn("Код", "Code"));

            DefaultFields.Add(new DictionaryEditorField("Название", "Name", typeof(string)));
            DefaultFields.Add(new DictionaryEditorField("Код", "Code", typeof(string)));
        }
    }
}
