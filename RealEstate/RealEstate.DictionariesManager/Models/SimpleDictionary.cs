﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstate.DictionariesManager.Controls;

namespace RealEstate.DictionariesManager.Models
{
    public class SimpleDictionary
    {
        public SimpleDictionary(Type entityType)
        {
            _entityType = entityType;

            _defaultColumns = new List<DictionaryGridColumn>();
            _defaultFields = new List<DictionaryEditorField>();

            _columns = new List<DictionaryGridColumn>();
            _fields = new List<DictionaryEditorField>();
        }

        private List<DictionaryGridColumn> _defaultColumns;
        public List<DictionaryGridColumn> DefaultColumns
        {
            get { return _defaultColumns; }
            set { _defaultColumns = value; }
        }

        private List<DictionaryGridColumn> _columns;
        public List<DictionaryGridColumn> Columns
        {
            get { return _columns; }
            set { _columns = value; }
        }

        private List<DictionaryEditorField> _defaultFields;
        protected List<DictionaryEditorField> DefaultFields
        {
            get { return _defaultFields; }
            set { _defaultFields = value; }
        }

        private List<DictionaryEditorField> _fields;
        public List<DictionaryEditorField> Fields
        {
            get { return _fields; }
            set { _fields = value; }
        }

        private Type _entityType;
        public Type EntityType
        {
            get { return _entityType ?? typeof(object); }
            set { _entityType = value; }
        }

        public DictionaryEditor NewEditor
        {
            get
            {
                var _editor = new DictionaryEditor(EntityType)
                {
                    Entity = Activator.CreateInstance(EntityType)
                };
                foreach (var field in DefaultFields)
                {
                    _editor.AddField(field.Label, field.DisplayMemberBinding, field.Type, field.GetItems);
                }
                foreach (var field in Fields)
                {
                    _editor.AddField(field.Label, field.DisplayMemberBinding, field.Type, field.GetItems);
                }
                return _editor;
            }
        }

        private DictionaryEditor _editor;
        public DictionaryEditor Editor
        {
            get 
            {
                if (_editor != null) return _editor;
                _editor = new DictionaryEditor(EntityType)
                    {
                        Entity = Activator.CreateInstance(EntityType)
                    };
                foreach (var field in DefaultFields)
                {
                    _editor.AddField(field.Label, field.DisplayMemberBinding, field.Type, field.GetItems);
                }
                foreach(var field in Fields)
                {
                    _editor.AddField(field.Label, field.DisplayMemberBinding, field.Type, field.GetItems);
                }
                return _editor; 
            }
            set { _editor = value; }
        }

        private DictionaryGrid _grid;
        public DictionaryGrid Grid
        {
            get 
            {
                if (_grid != null)
                {
                    _grid.Items = this.Items;
                    return _grid;
                }
                _grid = new DictionaryGrid(EntityType)
                    {
                        Items = this.Items
                    };
                foreach (var column in DefaultColumns)
                {
                    _grid.AddColumn(column.Header, column.DisplayMemberBinding);
                }
                foreach (var column in Columns)
                {
                    _grid.AddColumn(column.Header, column.DisplayMemberBinding);
                }
                return _grid; 
            }
            set { _grid = value; }
        }

        public Func<IEnumerable<object>> _getItems;
        public Func<IEnumerable<object>> GetItems
        {
            get { return _getItems; }
            set { _getItems = value; }
        }

        private IEnumerable<object> _items;
        public IEnumerable<object> Items
        {
            get { return _getItems(); }
            set { _items = value; }
        }
        
        public DictionaryEditor GetEditor(object entity)
        {
            var editor = Editor;
            editor.Entity = entity;
            return editor;
        }
        
    }
}
