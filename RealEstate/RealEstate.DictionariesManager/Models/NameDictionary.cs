﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstate.DictionariesManager.Models
{
    public class NameDictionary : SimpleDictionary
    {
        public NameDictionary(Type entityType) : base(entityType)
        {
            DefaultColumns.Add(new DictionaryGridColumn("Название", "Name"));

            DefaultFields.Add(new DictionaryEditorField("Название", "Name", typeof(string)));
        }
    }
}
