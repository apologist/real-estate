﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstate.DictionariesManager.Models
{
    public class DictionaryEditorField
    {
        public DictionaryEditorField(string label, string displayMemberBinding, Type type, Func<IEnumerable<object>> getItems = null)
        {
            _label = label;
            _displayMemberBinding = displayMemberBinding;
            _type = type;
            _getItems = getItems;
        }

        private string _label;
        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }

        private string _displayMemberBinding;
        public string DisplayMemberBinding
        {
            get { return _displayMemberBinding; }
            set { _displayMemberBinding = value; }
        }

        private Type _type;
        public Type Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private Func<IEnumerable<object>> _getItems;

        public Func<IEnumerable<object>> GetItems
        {
            get { return _getItems; }
            //set { _getItems = value; }
        }
        
        
        
        

    }
}
