﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstate.DictionariesManager.Controls;

namespace RealEstate.DictionariesManager.Events
{
    public class DictionaryGridEventArgs : EventArgs
    {
        private object _entity;
        public DictionaryGridEventArgs(object entity)
        {
            _entity = entity;
        }
        public object Entity
        {
            get { return _entity; }
        }
    }
}
