﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstate.DictionariesManager.Controls;

namespace RealEstate.DictionariesManager.Events
{
    public class DictionaryEditorEventArgs : EventArgs
    {
        private object _entity;

        public DictionaryEditorEventArgs(DictionaryEditor editor)
        {
            _entity = editor.Entity;

        }
        public object Entity
        {
            get { return _entity; }
        }
    }
}
