﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstate.DictionariesManager.Controls;

namespace RealEstate.DictionariesManager.Events
{
    public class DictionaryGridOpenEditorEventArgs : DictionaryGridEventArgs
    {
        private Func<DictionaryEditor> _getNewEditor;
        public DictionaryGridOpenEditorEventArgs(object entity, Func<DictionaryEditor> getNewEditor) : base(entity)
        {
            _getNewEditor = getNewEditor;
        }

        public DictionaryEditor NewEditor
        {
            get { return _getNewEditor(); }
        }
    }
}
