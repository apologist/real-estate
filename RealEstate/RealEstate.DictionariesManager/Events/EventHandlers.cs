﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstate.DictionariesManager.Events
{
    public delegate void DictionaryGridOpenEditorEventHandler(object source, DictionaryGridEventArgs e);

    public delegate void DictionaryEditorSave(object source, DictionaryEditorEventArgs e);
    public delegate void DictionaryEditorDelete(object source, DictionaryEditorEventArgs e);
    public delegate void DictionaryEditorClose(object source, DictionaryEditorEventArgs e);
    
}
