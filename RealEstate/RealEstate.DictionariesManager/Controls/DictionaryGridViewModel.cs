﻿using System;
using System.Windows.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstate.DictionariesManager.ViewModels;
using RealEstate.DictionariesManager.Interfaces;

namespace RealEstate.DictionariesManager.Controls
{
    public class DictionaryGridViewModel : BaseViewModel
    {
        public DictionaryGridViewModel()
        {
            EditorType = typeof(DictionaryEditor);
            //_grid = NewGridControl;
            //_mainContent = Grid;
        }

        #region Properties

        //private object _mainContent;
        //public object MainContent
        //{
        //    get { return _mainContent; }
        //    set
        //    {
        //        if (_mainContent != null && _mainContent.GetType() == typeof(DevExpress.Xpf.Grid.GridControl))
        //            Grid = _mainContent as DevExpress.Xpf.Grid.GridControl;
        //        _mainContent = value;
        //        RaisePropertyChanged(() => MainContent);
        //    }
        //}

        //private DevExpress.Xpf.Grid.GridControl NewGridControl
        //{
        //    get 
        //    {
        //        var gridControl = new DevExpress.Xpf.Grid.GridControl()
        //        {
        //            AutoPopulateColumns = true,
        //        };
        //        BindingBase bind = new Binding("Items") 
        //            { 
        //                Source = Items,
        //                Mode = BindingMode.TwoWay,
        //                UpdateSourceTrigger = UpdateSourceTrigger.LostFocus
        //            };
        //        gridControl.SetBinding(DevExpress.Xpf.Grid.GridControl.ItemsSourceProperty, bind);
        //        var tableView = new DevExpress.Xpf.Grid.TableView()
        //        {
        //            ShowTotalSummary = true,
        //        };
        //        tableView.RowDoubleClick += tableView_RowDoubleClick;
        //        gridControl.View = tableView;
        //        return gridControl;
        //    }
        //}

        //private DictionaryEditor _editor;
        //public DictionaryEditor Editor
        //{
        //    get { return _editor; }
        //    set { _editor = value; }
        //}

        //private DevExpress.Xpf.Grid.GridControl _grid;
        //public DevExpress.Xpf.Grid.GridControl Grid
        //{
        //    get { return _grid; }
        //    set { _grid = value; }
        //}

        //public DevExpress.Xpf.Grid.TableView TableView
        //{
        //    get
        //    {
        //        var view = Grid.View as DevExpress.Xpf.Grid.TableView;
        //        return view;
        //    }
        //}

        private Type _editorType;
        public Type EditorType
        {
            get { return _editorType; }
            set { _editorType = value; }
        }

        private IEnumerable<object> _items;
        public IEnumerable<object> Items
        {
            get { return _items; }
            set
            {
                _items = value;
                RaisePropertyChanged(() => Items);
                //Grid.ItemsSource = Items;
            }
        }

        #endregion

       
    }
}