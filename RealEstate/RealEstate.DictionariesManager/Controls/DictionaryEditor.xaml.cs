﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using RealEstate.DictionariesManager.Interfaces;
using RealEstate.DictionariesManager.Events;
using RealEstate.DictionariesManager.Helpers;

namespace RealEstate.DictionariesManager.Controls
{
    /// <summary>
    /// Interaction logic for DictionaryEditor.xaml
    /// </summary>
    public partial class DictionaryEditor : UserControl, IHasEntity//, INotifyPropertyChanged
    {
        public DictionaryEditor(Type entityType)
        {
            _entityType = entityType;
            this.DataContext = Activator.CreateInstance(entityType);
            InitializeComponent();
        }

        #region Properties

        private Type _entityType;
        public Type EntityType
        {
            get { return _entityType; }
            //set { _entityType = value; }
        }

        public object Entity
        {
            get 
            {
                return this.DataContext;
            }
            set 
            {
                this.DataContext = value;
            }
        }

        #endregion

        #region Methods

        public void AddField(string label, string displayMemberBinding, Type type, Func<IEnumerable<object>> getItems = null)
        {
            #region Label add
            var row = new RowDefinition();
            row.Height = new GridLength(0, GridUnitType.Auto);
            rootGrid.RowDefinitions.Add(row);

            var textBlock = new TextBlock()
                {
                    Text = label
                };
            Grid.SetColumn(textBlock, 1);
            Grid.SetRow(textBlock, rootGrid.RowDefinitions.Count-1);
            rootGrid.Children.Add(textBlock);
            #endregion

            var propertyBinding = new Binding(displayMemberBinding);

            if (type == typeof(string))
            {
                var textBox = new TextBox();
                textBox.SetBinding(TextBox.TextProperty, propertyBinding);

                Grid.SetColumn(textBox, 2);
                Grid.SetRow(textBox, rootGrid.RowDefinitions.Count - 1);
                rootGrid.Children.Add(textBox);
            }
            else if(type == typeof(List<object>))
            {
                var comboBox = new ComboBox();

                if (getItems != null)
                    comboBox.ItemsSource = getItems();
                comboBox.DisplayMemberPath = "Name";

                comboBox.SetBinding(ComboBox.SelectedItemProperty, propertyBinding);
                
                
                //comboBox.SetBinding(ComboBox.SelectionBoxItemProperty, bind);

                Grid.SetColumn(comboBox, 2);
                Grid.SetRow(comboBox, rootGrid.RowDefinitions.Count - 1);
                rootGrid.Children.Add(comboBox);
            }

        }

        private void SaveChanges()
        {
            if(Save != null)
                Save(this, new DictionaryEditorEventArgs(this));
        }

        #endregion

        #region Events

        public event DictionaryEditorSave Save;
        public event DictionaryEditorClose Close;

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            if (Close != null)
                Close(this, new DictionaryEditorEventArgs(this));
        }

        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            SaveChanges();
        }
        #endregion

        

        
    }
}
