﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RealEstate.DictionariesManager.Events;
using RealEstate.DictionariesManager.Helpers;

namespace RealEstate.DictionariesManager.Controls
{
    /// <summary>
    /// Interaction logic for DictionaryGrid.xaml
    /// </summary>
    /// 

    public partial class DictionaryGrid : UserControl
    {
        public DevExpress.Xpf.Grid.GridControl GridControl
        {
            get { return gridControl; }
        }

        public DictionaryGrid(Type entityType)
        {
            DataContext = new DictionaryGridViewModel();
            _entityType = entityType;
            InitializeComponent();
        }

        #region Properties

        private Type _entityType;
        public Type EntityType
        {
            get { return _entityType; }
            set
            {
                _entityType = value;
            }
        }

        private IEnumerable<object> _items;
        public IEnumerable<object> Items
        {
            get { return _items; }
            set 
            { 
                _items = value;
                gridControl.ItemsSource = value;
            }
        }

        #endregion

        #region Methods


        public void AddColumn(string header, string displayMemberBinding, string memberPropertyPath = null, double width = -1)
        {
            if (memberPropertyPath != null)
                displayMemberBinding += "." + memberPropertyPath;
            var column = new DevExpress.Xpf.Grid.GridColumn()
            {
                Header = header,
                DisplayMemberBinding = new Binding(displayMemberBinding),
            };
            if(width != -1) 
                column.Width = width;
            gridControl.Columns.Add(column);
        }

        #endregion

        #region Events

        public event DictionaryGridOpenEditorEventHandler OpenEditor;

        private void tableView_RowDoubleClick(object sender, DevExpress.Xpf.Grid.RowDoubleClickEventArgs e)
        {
            var view = (e.Source as DevExpress.Xpf.Grid.TableView);
            int rowHandle = e.HitInfo.RowHandle;
            

            if (view != null)
            {
                var entity = view.Grid.GetRow(rowHandle);
                if(OpenEditor != null)
                    OpenEditor(this, new DictionaryGridEventArgs(entity));
            }
        }

        #endregion
    }
}
