﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstate.DataAccess;
using System.Windows.Input;
using RealEstate.DictionariesManager.Helpers;
using RealEstate.DictionariesManager.Controls;
using RealEstate.DictionariesManager.Events;
using RealEstate.DictionariesManager.Models;
using RealEstate.DictionariesManager.Dictionaries;

namespace RealEstate.DictionariesManager.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        #region Commands

        public ICommand OpenDictionaryCommand
        {
            get 
            { return new DelegateCommand((p) => LoadDictionaryAndSetDictionaryItems(p as SimpleDictionary)); }
        }

        public ICommand AddNewEntityCommand
        {
            get { return new DelegateCommand(() => AddNewEntity(), CanAddNewEntity); }
        }

        public ICommand EditEntityCommand
        {
            get { return new DelegateCommand(() => EditEntity(), CanEditEntity); }
        }

        public ICommand DeleteEntityCommand
        {
            get { return new DelegateCommand(() => DeleteEntity(), CanDeleteEntity); }
        }

        #endregion

        #region Properties

        private SimpleDictionary _currentDictionary;
        public SimpleDictionary CurrentDictionary
        {
            get 
            {
                return _currentDictionary; 
            }
            set 
            {
                _currentDictionary = value;

                CurrentDictionary.Grid.OpenEditor += new DictionaryGridOpenEditorEventHandler(DictionaryGridOpenEditor);

                CurrentDictionary.Editor.Save += new DictionaryEditorSave(DictionaryEditorSave);
                CurrentDictionary.Editor.Close += new DictionaryEditorClose(DictionaryEditorClose);

                RaisePropertyChanged(() => CurrentDictionary);
            }
        }

        private object _mainContent;
        public object MainContent
        {
            get
            {
                return _mainContent;
            }
            set
            {
                _mainContent = value;
                RaisePropertyChanged(() => MainContent);
            }
        }

        #endregion

        #region Methods

        private void LoadDictionaryAndSetDictionaryItems(SimpleDictionary dictionary)
        {
            if (dictionary != null)
            {
                CurrentDictionary = dictionary;
                MainContent = CurrentDictionary.Grid;
            }
        }

        private void AddNewEntity()
        {
            if(CurrentDictionary != null)//gag
            {
                CurrentDictionary.Editor.Entity = Activator.CreateInstance(CurrentDictionary.EntityType);
                MainContent = CurrentDictionary.Editor;
            }
        }

        private bool CanAddNewEntity()
        {
            return CurrentDictionary != null;
        }

        private void EditEntity()
        {
            var entity = CurrentDictionary.Grid.GridControl.GetFocusedRow();
            if(entity != null)
                MainContent = CurrentDictionary.GetEditor(entity);
        }

        private bool CanEditEntity()
        {
            return CurrentDictionary != null;
        }

        private void DeleteEntity()
        {
            var entity = CurrentDictionary.Grid.GridControl.GetFocusedRow();
            if (entity != null)
                try
                {
                    Api.Delete(entity);
                    MainContent = CurrentDictionary.Grid;
                }
                catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
                {
                    DevExpress.Xpf.Core.DXMessageBox.Show("Сущность не может быть удалена потому что другие объекты ссылаются на нее." + Environment.NewLine + ex.Message, "Ошибка!");
                }
                catch (Exception ex)
                {
                    DevExpress.Xpf.Core.DXMessageBox.Show(ex.Message, "Ошибка!");
                }
        }

        private bool CanDeleteEntity()
        {
            return CurrentDictionary != null;
        }

        private void DictionaryGridOpenEditor(object source, DictionaryGridEventArgs e)
        {
            MainContent = CurrentDictionary.GetEditor(e.Entity);
        }

        private void DictionaryEditorSave(object source, DictionaryEditorEventArgs e)
        {
            try
            {
                Api.Save(e.Entity);
                MainContent = CurrentDictionary.Grid;
            }
            catch (Exception ex)
            {
                DevExpress.Xpf.Core.DXMessageBox.Show(ex.Message, "Ошибка!");
            }
            
        }

        private void DictionaryEditorClose(object source, DictionaryEditorEventArgs e)
        {
            MainContent = CurrentDictionary.Grid;
        } 

        #endregion

        #region Dictionaries

        public SimpleDictionary CountryDictionary
        {
            get { return new NameDictionary(typeof(Country)) { GetItems = Api.GetCountries }; }
        }

        public SimpleDictionary RegionDictionary
        {
            get 
            {
                return new NameDictionary(typeof(Region))
                {
                    GetItems = Api.GetRegions,
                    Columns = new List<DictionaryGridColumn>() { new DictionaryGridColumn("Страна", "Country.Name") },
                    Fields = new List<DictionaryEditorField>() { new DictionaryEditorField("Страна", "Country", typeof(List<object>), Api.GetCountries) }
                }; 
            }
        }

        public SimpleDictionary LocalityDictionary
        {
            get
            {
                return new NameDictionary(typeof(Locality))
                {
                    GetItems = Api.GetLocalities,
                    Columns = new List<DictionaryGridColumn>() 
                    { 
                        new DictionaryGridColumn("Страна", "Country.Name"), 
                        new DictionaryGridColumn("Область", "Region.Name"),
                        new DictionaryGridColumn("Тип населенного пункта", "LocalityType.Name"),
                    },
                    Fields = new List<DictionaryEditorField>() 
                    { 
                        new DictionaryEditorField("Страна", "Country", typeof(List<object>), Api.GetCountries),
                        new DictionaryEditorField("Область", "Region", typeof(List<object>), Api.GetRegions),
                        new DictionaryEditorField("Тип населенного пункта", "LocalityType", typeof(List<object>), Api.GetLocalityTypes),
                    }
                };
            }
        }

        public SimpleDictionary DistrictDictionary
        {
            get
            {
                return new NameDictionary(typeof(District))
                {
                    GetItems = Api.GetDistricts,
                    Columns = new List<DictionaryGridColumn>() 
                    { 
                        new DictionaryGridColumn("Страна", "Locality.Country.Name"), 
                        new DictionaryGridColumn("Область", "Locality.Region.Name"),
                        new DictionaryGridColumn("Населенный пункт", "Locality.Name"),
                    },
                    Fields = new List<DictionaryEditorField>() 
                    { 
                        new DictionaryEditorField("Населенный пункт", "Locality", typeof(List<object>), Api.GetLocalities),
                    }
                };
            }
        }

        public SimpleDictionary MicroDistrictDictionary
        {
            get
            {
                return new NameDictionary(typeof(MicroDistrict))
                {
                    GetItems = Api.GetMicroDistricts,
                    Columns = new List<DictionaryGridColumn>() 
                    { 
                        new DictionaryGridColumn("Страна", "District.Locality.Country.Name"), 
                        new DictionaryGridColumn("Область", "District.Locality.Region.Name"),
                        new DictionaryGridColumn("Населенный пункт", "District.Locality.Name"),
                        new DictionaryGridColumn("Район", "District.Name"),
                    },
                    Fields = new List<DictionaryEditorField>() 
                    { 
                        new DictionaryEditorField("Район", "District", typeof(List<object>), Api.GetDistricts),
                    }
                };
            }
        }

        public SimpleDictionary StreetDictionary
        {
            get
            {
                return new NameDictionary(typeof(Street))
                {
                    GetItems = Api.GetStreets,
                    Columns = new List<DictionaryGridColumn>() 
                    { 
                        new DictionaryGridColumn("Страна", "MicroDistrict.District.Locality.Country.Name"), 
                        new DictionaryGridColumn("Область", "MicroDistrict.District.Locality.Region.Name"),
                        new DictionaryGridColumn("Населенный пункт", "MicroDistrict.District.Locality.Name"),
                        new DictionaryGridColumn("Район", "MicroDistrict.District.Name"),
                        new DictionaryGridColumn("Микрорайон", "MicroDistrict.Name"),
                    },
                    Fields = new List<DictionaryEditorField>() 
                    { 
                        new DictionaryEditorField("Населенный пункт", "Locality", typeof(List<object>), Api.GetLocalities),
                        new DictionaryEditorField("Район", "District", typeof(List<object>), Api.GetDistricts),
                        new DictionaryEditorField("Микрорайон", "MicroDistrict", typeof(List<object>), Api.GetMicroDistricts),
                    }
                };
            }
        }

        public SimpleDictionary LocalityTypeDictionary
        {
            get { return new StandardDictionary(typeof(LocalityType)) { GetItems = Api.GetLocalityTypes }; }
        }

        public SimpleDictionary ObjectStatusDictionary
        {
            get { return new StandardDictionary(typeof(ObjectStatus)) { GetItems = Api.GetObjectStatuses }; }
        }

        public SimpleDictionary CurrencyDictionary
        {
            get { return new StandardDictionary(typeof(Currency)) { GetItems = Api.GetCurrencies }; }
        }

        public SimpleDictionary ContractTypeDictionary
        {
            get { return new StandardDictionary(typeof(ContractType)) { GetItems = Api.GetContractTypes }; }
        }

        public SimpleDictionary RealtyTypeDictionary
        {
            get { return new StandardDictionary(typeof(RealtyType)) { GetItems = Api.GetRealtyTypes }; }
        }

        public SimpleDictionary RoomTypeDictionary
        {
            get { return new StandardDictionary(typeof(RoomType)) { GetItems = Api.GetRoomTypes }; }
        }

        public SimpleDictionary HouseTypeDictionary
        {
            get { return new StandardDictionary(typeof(HouseType)) { GetItems = Api.GetHouseTypes }; }
        }

        public SimpleDictionary WcTypeDictionary
        {
            get { return new StandardDictionary(typeof(WcType)) { GetItems = Api.GetWcTypes }; }
        }

        public SimpleDictionary BalconyTypeDictionary
        {
            get { return new StandardDictionary(typeof(BalconyType)) { GetItems = Api.GetBalconyTypes }; }
        }

        #endregion
    }
}

