using System;
using System.Windows.Input;

namespace RealEstate.DictionariesManager.Helpers
{
    public class DelegateCommand : ICommand
    {
        private readonly Action _command;
        private readonly Action<object> _commandWithParameter;
        private readonly Func<bool> _canExecute;
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public DelegateCommand(Action command, Func<bool> canExecute = null)
        {
            if (command == null)
                throw new ArgumentNullException();
            _canExecute = canExecute;
            _command = command;
        }

        public DelegateCommand(Action<object> command, Func<bool> canExecute = null)
        {
            if (command == null)
                throw new ArgumentNullException();
            _canExecute = canExecute;
            _commandWithParameter = command;
        }

        public void Execute(object parameter)
        {
            if (_command != null) _command();
            else _commandWithParameter(parameter);
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute();
        }

    }
}